---
title: "Et si nous écrivions ici un titre assez long d'un article du blog ?"
date: 2019-07-21T10:51:20+02:00
image: "/images/droneUne.jpg" #IMAGE SERVANT A L'ILLUSTRATION DE l'ARTICLE
categories : "Le monde du digital"
tag: ""
description: ""
type: "article" #DOSSIER CONTENANT LE LAYOUT (MISE EN PAGE)
draft: false
---

<!-- CONTENU DE L'ARTICLE (METTRE DES <br> POUR FAIRE UN SAUT DE LIGNE-->
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. 
<br><br>
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.
<br/><br/>

<!-- SYNTAXE IMAGE ![Nom Image][/images/nomImage#positionnement de l'image] -->
<!-- SYNTAXE IMAGE HEBERGE EN LIGNE ![Nom Image][URLImage#positionnement de l'image] -->
<!-- POSTIONNEMENT DE L'IMAGE = floatleft / center / floatright -->
![Image Voiture](/images/droneUne.jpg#center)
<br/><br/>

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. 
<br/><br/>
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.
<br/><br/>
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. 
<br/><br/>
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.